int PINO = 9;            // Digital IO pin connected to base of transistor
int Length = 537;        // Length in Microseconds
int DebugOn = 0;

#define POWERNOFF 0x00
#define VOLUP 0x04
#define VOLDOWN 0x05
#define MUTE 0x06
#define SOURCE 0x08
#define EQUALIZER 0x0d///0x0C
#define MUTE2 0x0e//0x0D// - Mute/Unmute (and Power OFF with "press and hold" - see below)
#define SEARCHFORW 0x12// - Seach + / Track + (and Manual Tune + / Fast fwd with "press and hold")
#define SEARCHBACK 0x13// - Seach - / Track - (and Manual Tune - / Fast rwd with "press and hold")
#define BANDFORW 0x14// - Band cycle / Folder + //programs
#define BANDBACK 0x15// - Band cycle / Folder + //programs
#define PROGR 0x15// - Program 1-6 cycle / Folder -//programs
//#define UNKNOWN 0x16// - Sel cycle (it cycles between Fader/Balance/Loudness/Subwoofer out level/Volume gain for the current source)

//#define GREENPIN 5
//#define BLUEPIN 3
//#define YELLOWPIN 6
//#define BROWNPIN 7
//#define REDPIN 4
//#define BLACKPIN 2

#define GREENPIN 6
#define BLUEPIN 4
#define YELLOWPIN 7
#define BROWNPIN 8
#define REDPIN 5
#define BLACKPIN 3

#define BLUETOOTHPIN 2

#define OUT_PINS 3
unsigned char out_pins[OUT_PINS]={
  GREENPIN,BLUEPIN,YELLOWPIN};
unsigned char Key = 0;

void setup() {
  pinMode(PINO, OUTPUT);
  digitalWrite(PINO, LOW); // Make PIN low to shut off transistor
  Serial.begin(115200);
  Serial.println("Hello!");
    
  pinMode(BLUETOOTHPIN, OUTPUT);
  digitalWrite(BLUETOOTHPIN, HIGH);
  
  
  pinMode(GREENPIN, OUTPUT);
  pinMode(BLUEPIN, OUTPUT);
  pinMode(YELLOWPIN, OUTPUT);
  pinMode(BROWNPIN, INPUT_PULLUP);
  pinMode(REDPIN, INPUT_PULLUP);
  pinMode(BLACKPIN, INPUT_PULLUP);
  Key = GetJoystic();
  delay(4000);
  Key = GetJoystic();
  digitalWrite(BLUETOOTHPIN, LOW);
  
  Serial.println("Enter 1 to Go");
  
}

unsigned char GetJoystic(void){
  static unsigned char stage = 0;
  static unsigned char scroll_stored = 0;
  unsigned char tmp,i;
  if (++stage > (OUT_PINS-1)) stage=0;

  for (i=0;i<(OUT_PINS);i++)
    if (i==stage){
      digitalWrite(out_pins[i], LOW);
    }
  else{
    digitalWrite(out_pins[i], HIGH);
  }

  delay(10);

  tmp = digitalRead(BROWNPIN);
  if (!tmp){
    if (stage != scroll_stored){
      char scrl = stage-scroll_stored;
      scroll_stored = stage;
      if ((scrl == 1) ||(scrl==-2)) {
        Serial.println("Scroll-");
        return SEARCHFORW;
      }else{
        Serial.println("Scroll+");
        return SEARCHBACK;
      }
    }
  }
  tmp = digitalRead(REDPIN);
  if (!tmp){
    switch(stage){
    case 0:
      Serial.println("Select");
      return SOURCE;
    case 1:
      Serial.println("Vol+");
      return VOLUP;
    case 2:
      Serial.println("Vol-");
      return VOLDOWN;
    } 
  }
  tmp = digitalRead(BLACKPIN);
  if (!tmp){
    switch(stage){
    case 0:
      Serial.println("Source-");
      return BANDFORW;
    case 1:
      Serial.println("Mute");
      return MUTE2;
    case 2:
      Serial.println("Source+");
      return BANDBACK;
    }
  }
  return 0;
}

void loop() {
  Key = GetJoystic();
  static unsigned char code = 0;
  if (Key){
    Serial.print("Key ");
//    Serial.println(Key,HEX);
    JVCSendCommand(Key);
    delay(2);
    JVCSendCommand(Key);
    delay(20);
  }

  if (Serial.available() > 0) {
    char inp=Serial.read();
    Serial.print("Received: ");
    Serial.println(inp, DEC);
    switch (inp){
      case '1':
      code--;
      break;
      case '2':
      code++;
      break;
      case '3':
      JVCSendCommand(code);
      break;
    }
  
  
  }
}  // end of loop


void JVCSendCode(unsigned char code){
  unsigned char i,tmp=1;
  for (i=0;i<sizeof(code)*8-1;i++){//7bits
    if (code & tmp)
      bONE();
    else
      bZERO();
    tmp = tmp << 1;
  }
}

void JVCSendCommand(unsigned char code){
  Preamble();
  JVCSendCode((unsigned char)code);
  Postamble();
}

//    Wire signals to be generated for a '1' bit
void bONE() {                     // Send a binary ONE over the line
  if (DebugOn == 1) {
    Serial.print(" 1 ");
  }
  digitalWrite(PINO, HIGH);        // Pull 3.5mm TIP low
  delayMicroseconds(Length);      // for 537us
  digitalWrite(PINO, LOW);         // Allow 3.5mm TIP to go high
  delayMicroseconds(Length * 3);  // for 537 * 3 = 1611us
}

//    Wire signals to be generated for a '0' bit
void bZERO() {   // Send a binary ZERO over the line
  if (DebugOn == 1) {
    Serial.print(" 0 ");
  }
  digitalWrite(PINO, HIGH);        // Pull 3.5mm TIP low
  delayMicroseconds(Length);      // for 537us
  digitalWrite(PINO, LOW);         // Allow 3.5mm TIP to go high
  delayMicroseconds(Length);      // for 537us
}

//    Wire signals to be generated for a start of signal to a JVC
void Preamble() {
  if (DebugOn == 1) {
    Serial.println(" ");
    Serial.print(" AGC ");
  }
  digitalWrite(PINO, LOW);         // Not sure what this does
  delayMicroseconds(Length * 1);

  digitalWrite(PINO, HIGH);        // AGC
  delayMicroseconds(Length * 16);

  digitalWrite(PINO, LOW);         // AGC
  delayMicroseconds(Length * 8);

  bONE();    // 1 Start Bit
  JVCSendCode(0x47);
}

//    Wire signals to be generated for a end of signal  
void Postamble() {
  if (DebugOn == 1) {
    Serial.print(" STOP ");
  }
  bONE();
  bONE();    // 2 stop bits
}
